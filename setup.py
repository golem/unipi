import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="unipi",
    version="0.0.1",
    author="Ondrej Grover",
    author_email="ondrej.grover@gmail.com",
    description="UniPi interface used at the GOLEM tokamak",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.fjfi.cvut.cz/golem/unipi",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 2",
    ],
    install_requires=['six', 'smbus', 'RPi.GPIO'],
)
