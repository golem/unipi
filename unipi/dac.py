"""Analog Output (DAC) controller for UniPi v1.1


Requires an external DC power supply (max 35 V) between the AOG (ground/-) and AOV (+) connectors.
The output voltage is between the AO(+) and AOG(-) connectors.

The output voltage is controlled by software PWM, so it works only while the program runs
"""
from RPi import GPIO

GPIO.setmode(GPIO.BCM)


AO_GPIO_PIN = 18


def voltage_to_duty_cycle(voltage):
    """Voltage must be in range 0-10 V

    For UniPi v1.1: 0 V is 0.0 duty cycle, 10 V is 100.0 duty cycle
    """
    if voltage < 0 or voltage > 10:
        raise ValueError('voltage {} not in range 0-10')
    return voltage * 10.0


class AnalogOutput(object):
    """Object wrapping the PWM controller

    Because this uses software PWM,
    the controller only works while this object exists.
    """

    def __init__(self, initial_voltage=0):
        """Initiates the PWM controller with some initial voltage

        """
        GPIO.setup(AO_GPIO_PIN, GPIO.OUT)
        self.pwm = GPIO.PWM(AO_GPIO_PIN, 400)  # 400 Hz PWM
        self.pwm.start(voltage_to_duty_cycle(initial_voltage))

    def set(self, voltage):
        """Set the voltage in the range 0-10 V"""
        self.pwm.ChangeDutyCycle(voltage_to_duty_cycle(voltage))


if __name__ == '__main__':
    import argparse
    from time import sleep
    parser = argparse.ArgumentParser(
        description='set analog output to a given voltage for a given time')
    parser.add_argument('voltage', help='target analog output voltage in range 0-10 V', type=float)
    parser.add_argument('hold', help='for how many seconds to hold the voltage', type=float)
    args = parser.parse_args()
    ao = AnalogOutput(args.voltage)
    print('Holding voltage for {} seconds'.format(args.hold))
    sleep(args.hold)
