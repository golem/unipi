# UniPi controls for RaspberryPi devices

Utility modules for use at the GOLEM tokamak

## Installation 

    pip3 install git+https://<url of this repo page>
    
### Installation for development

Clone this repository and then install it as is

    git clone https://<url of this repo page>
    cd unipi
    pip3 install -e .
    
## Usage on the command-line

Read given analog input (AI)

    python3 -m unipi.adc.reader 1
    
Start the relay control GUI

    python3 -m unipi.relays.gui
    
Set the analog output voltage to 5.6 V for 3.3 seconds
(Note: requires external voltage supply, see the `unipi.dac` module docstring)

    python3 -m unipi.dac 5.6 3.3
    
Read info from the EEPROM

    sudo python3 -m unipi.eeprom
    
## Usage in Python 3

Create analog input interface

    from unipi.adc.reader import AI
    ai1 = AI(1)
    value = ai1.read()
    
Create a relay controller interface

    from unipi.relays.controller import RelaysController
    c = RelaysController()
    c.relay(1, True)  # turn on relay 1
    
Create an output voltage controller with an initial voltage of 3.3 V
and then set it to 5.3 V
(Note: requires external voltage supply, see the module docstring)

    from unipi.dac import AnalogOutput
    ao = AnalogOutput(3.3)
    ao.set(5.3)
    
In the future these interfaces may be merged into 1.

## Sources

- [python-MCP342x](https://github.com/stevemarple/python-MCP342x): Python2 package for reading the ADC chip, converted to be Python 3 compatible
- official [UniPi technology API server EVOK](https://github.com/UniPiTechnology/evok): overkill JSON, REST API server, EEPROM reading functionality was extracted
